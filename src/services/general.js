export {
  currency,
  capitalize,
  scrollingOptions
}

function currency(nbr) {
  return "$" + nbr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function capitalize(str) {
  if (str.length) {
    return str.charAt(0).toUpperCase() + str.substring(1);
  } else {
    return ''
  }
}

function scrollingOptions(offset) {
  return {
    duration: 700,
    offset: offset,
    easing: "easeInOutCubic",
  }
}


