import { federalTaxBrackets, qcTaxBrackets, onTaxBrackets, bcTaxBrackets } from "./taxBrackets"

export{
  estimateGross,
  neededNet,
  neededCapital,
  resultCashflow
}

function getBrackets(state) {
  if (state === "bc") { return bcTaxBrackets }
  if (state === "on") { return onTaxBrackets }
  if (state === "qc") { return qcTaxBrackets }
  if (state === "fe") { return federalTaxBrackets }
}

function divideIntoBrackets(amount, state) {
  let initialAmount = amount
  let brackets = getBrackets(state)
  let splitAmount = []
  for (let i = 0; i < brackets.length - 1; i++) {
    let diff = brackets[i].interval[1] - brackets[i].interval[0]
    amount = amount - diff
    if (amount <= 0) { 
      splitAmount.push(amount + diff)
      return splitAmount 
    }
    else {
      splitAmount.push(diff)
    }
  }
  splitAmount.push(initialAmount - (brackets[brackets.length - 1].interval[0]))
  return splitAmount
}

function calculateTaxes(amount, state) {
  let percentages = getBrackets(state).map(x => x.tax)
  let splits = divideIntoBrackets(amount, state)

  return splits.map((x,i) => x*percentages[i]).reduce((a,b) => a + b, 0)
}

function getNetIncome(amount, state) {
  let federal = calculateTaxes(amount, "fe")
  let local = calculateTaxes(amount, state)
  return (amount - (federal + local)) * 1.0
}

function estimateGross(net, state) {
 let coeff = 1.3
 let finished = false
 while(!finished) {
  let trialNet = getNetIncome(net*coeff, state)
  let diff = trialNet - net
  if (diff < -1) { coeff = coeff + Math.abs(1 - trialNet/net) } 
  if (diff > 1) { coeff = coeff - Math.abs(1 - trialNet/net) }
  if (diff >= -1 && diff <= 1) { 
    finished = true 
  }
 }
 return Math.ceil(net*coeff)
}

function neededNet(rules) {
  let sumAmounts = [...rules].filter(y => y.type === "amount")
  if (sumAmounts) {
    sumAmounts = [...sumAmounts].map(x => {
      if (x.frequency === "month") { return x.nbr }
      if (x.frequency === "year") { return x.nbr/12 }
    }).reduce((a,b) => a + b, 0)
  }
  let sumPercentages = [...rules].filter(y => y.type === "percentage")
  if (sumPercentages) {
    sumPercentages = [...sumPercentages].map(x => {
      if (x.frequency === "month") { return x.nbr/100 }
      if (x.frequency === "year") { return x.nbr/1200 }
    }).reduce((a,b) => a + b, 0)
  }

  return Math.ceil((sumAmounts/(1 - sumPercentages))*12)
}


// =============================================================

function neededCapital(obj) {
  // Fields for type = capital: [type, amount, frequency, interestRate ]
  return Math.ceil((obj.amount * oneYearMultiple(obj.frequency)) / obj.interestRate)
}

function resultCashflow(obj) {
  // Fields for type = cashflow: [type, amount, interestRate, duration*, durationUnit*]
  let cf = obj.amount * obj.interestRate
  let res = {
    yearly: Math.ceil(cf),
    monthly: Math.ceil(cf / oneYearMultiple("month")),
    weekly: Math.ceil(cf / oneYearMultiple("week"))
  }
  if (obj.durationUnit) {
    res["total"] = Math.ceil((cf / oneYearMultiple(obj.durationUnit)) * obj.duration)
  }
  return res
}

function oneYearMultiple(freq) {
  if (freq.includes("day")) { return 365  }
  if (freq.includes("week")) { return 365/7  }
  if (freq.includes("month")) { return 12 }
  if (freq.includes("year")) { return 1 }
}
