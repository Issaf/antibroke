export {
  federalTaxBrackets,
  qcTaxBrackets,
  onTaxBrackets,
  bcTaxBrackets
}

const federalTaxBrackets = [
  {interval: [0, 47630], tax: .15},
  {interval: [47630, 95259], tax: .205},
  {interval: [95259, 147667], tax: .26},
  {interval: [147667, 210371], tax: .29},
  {interval: [210371, Infinity], tax: .33}
]

const qcTaxBrackets = [
  {interval: [0, 43790], tax: .15},
  {interval: [43790, 87575], tax: .20},
  {interval: [87575, 106555], tax: .24},
  {interval: [106555, Infinity], tax: .2575}
]

const onTaxBrackets = [
  {interval: [0, 43906], tax: .0505},
  {interval: [43906, 87813], tax: .0915},
  {interval: [87813, 150000], tax: .1116},
  {interval: [150000, 220000], tax: .1216},
  {interval: [220000, Infinity], tax: .1316}
]

const bcTaxBrackets = [
  {interval: [0, 40707], tax: .0506},
  {interval: [40707, 81416], tax: .077},
  {interval: [81416, 93476], tax: .105},
  {interval: [93476, 113506], tax: .1229},
  {interval: [113506, 153900], tax: .147},
  {interval: [153900, Infinity], tax: .168}
]