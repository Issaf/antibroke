import Vue from 'vue'
import Router from 'vue-router'
import Main from './views/Main.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/salary-calculator',
    },
    {
      path: '/salary-calculator',
      alias: ['/cashflow-calculator'],
      name: 'main',
      component: Main
    },
    {
      path: '*',
      redirect: '/salary-calculator'
    }
  ]
})
