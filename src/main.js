import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false
Vue.prototype.desktopMarginTop = 137
Vue.prototype.mobileMarginTop = 60
Vue.prototype.globalColors = {
  yellow: "#f7be16",
  darkTeal: "#00818a",
  darkBlue: "#216583",
  darkPurple: "#293462"
}

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
